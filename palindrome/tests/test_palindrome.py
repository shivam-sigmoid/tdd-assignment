import unittest
from unittest.mock import patch, mock_open
from palindrome.app.palindrome import Palindrome


pal = Palindrome()



class TestPalindromes(unittest.TestCase):
    # Test Runner will run this method prior to each test
    def setUp(self):
        self.mock_data = 'LOL'
        self.mock_data_mirrored_strings = ['', 'A', 'BB', 'LOL', 'NOON', 'radar', 'racecar']

    def test_set_up_tear_down(self):
        assert pal.is_palindrome(self.mock_data) is True

    # Test Runner will invoke that method after each test
    def tearDown(self):
        self.mock_data = ''
        self.mock_data_mirrored_strings = []

    def test_mock_read_from_file(self):
        with patch("builtins.open", mock_open(read_data="data")) as mock_file:
            assert open("path/to/open").read() == "data"
        mock_file.assert_called_with("path/to/open")

    def test_is_palindrome_with_mirrored_strings(self):
        # simple palindromes that are mirrored strings
        for data in self.mock_data_mirrored_strings:
            assert pal.is_palindrome(data) is True
        # assert is_palindrome('') is True  # base case
        # assert is_palindrome('A') is True  # base case

    def test_is_palindrome_with_mixed_casing(self):
        # palindromes with mixed letter casing
        assert pal.is_palindrome('Bb') is True
        assert pal.is_palindrome('NoOn') is True
        assert pal.is_palindrome('Radar') is True
        assert pal.is_palindrome('RaceCar') is True

    def test_is_palindrome_with_whitespace(self):
        # palindromes with whitespace
        assert pal.is_palindrome('taco cat') is True
        assert pal.is_palindrome('race car') is True
        assert pal.is_palindrome('race fast safe car') is True

    def test_is_palindrome_with_whitespace_and_mixed_casing(self):
        # palindromes with whitespace and mixed letter casing
        assert pal.is_palindrome('Taco Cat') is True
        assert pal.is_palindrome('Race Car') is True
        assert pal.is_palindrome('Race Fast Safe Car') is True

    def test_is_palindrome_with_whitespace_and_punctuation(self):
        # palindromes with whitespace and punctuation
        assert pal.is_palindrome('taco cat!') is True
        assert pal.is_palindrome('race, car!!') is True
        assert pal.is_palindrome('race fast, safe car.') is True

    def test_is_palindrome_with_mixed_casing_and_punctuation(self):
        # palindromes with whitespace, punctuation and mixed letter casing
        assert pal.is_palindrome('Race fast, safe car.') is True
        assert pal.is_palindrome('Was it a car or a cat I saw?') is True
        assert pal.is_palindrome("Go hang a salami, I'm a lasagna hog.") is True
        assert pal.is_palindrome('A man, a plan, a canal - Panama!') is True

    def test_is_palindrome_with_non_palindromic_strings(self):
        assert pal.is_palindrome('AB') is False  # even length
        assert pal.is_palindrome('ABC') is False  # odd length
        assert pal.is_palindrome('doge') is False
        assert pal.is_palindrome('monkey') is False
        assert pal.is_palindrome('chicken, monkey!') is False

    def test_is_palindrome_not_palindrome(self):
        assert not pal.is_palindrome('abc')

    def test_is_palindrome_not_quite(self):
        assert not pal.is_palindrome("abab")


if __name__ == '__main__':
    unittest.main()
