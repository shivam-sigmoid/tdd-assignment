from palindrome.app.palindrome import Palindrome
from unittest.mock import patch, mock_open
from unittest.mock import MagicMock
pal = Palindrome()
import pytest


# @pytest.fixture()
# def palindrome():
#     pal = Palindrome()
#     return pal


def test_mock_read_from_file():

    with patch("builtins.open", mock_open(read_data="data")) as mock_file:
        assert open("path/to/open").read() == "data"
    mock_file.assert_called_with("path/to/open")


#
# def test_mock_read_from_magic_mock(palindrome,monkeypatch):
#     mock_file = MagicMock()
#     mock_file.readlines = MagicMock(return_value="testdata")
#     mock_open = MagicMock(return_value=mock_file)
#     monkeypatch.setattr("builtins.open", mock_open)
#     result = palindrome.read_from_file("tes")
#     mock_open.assert_called_once_with("tes", 'r')
#     assert result == "testdata"



# class FileReader:
#     @staticmethod
#     def read_content(file_path):
#         with open(file_path, 'r') as _file:
#             file_content_list = _file.readlines()
#             return file_content_list
#
# def test_count_lines():
#     file_content_mock = "True"
#     fake_file_path = 'file/path/mock'
#
#     with patch('/Users/shivamraj/Documents/Learning/pytest/palindrome/resources/file_one.txt'.format(__name__),
#                new=mock_open(read_data=file_content_mock)) as _file:
#         actual = FileReader().read_content(fake_file_path)_file.assert_called_once_with(fake_file_path, 'r')
#         expected = file_content_mock
#         assert actual==expected

@pytest.fixture
def input_from_file_one():
    file = open("/Users/shivamraj/Documents/Learning/pytest/palindrome/resources/file_one.txt", "r")
    examples = file.read()
    return examples


@pytest.fixture
def output_from_file_one():
    file = open("/Users/shivamraj/Documents/Learning/pytest/palindrome/resources/expected_file_one.txt", "r")
    examples = file.read()
    return examples


def test_is_palindrome_from_file_one(input_from_file_one, output_from_file_one):
    assert str(pal.is_palindrome(input_from_file_one)) == output_from_file_one


@pytest.mark.parametrize("palindrome", [
    "",
    "a",
    "Bob",
    "Never odd or even",
    "Do geese see God?",
])
def test_is_palindrome(palindrome):
    assert pal.is_palindrome(palindrome)


@pytest.fixture
def input_from_file_two():
    file = open("/Users/shivamraj/Documents/Learning/pytest/palindrome/resources/file_two.txt", "r")
    examples = file.read()
    return examples


@pytest.fixture
def output_from_file_two():
    file = open("/Users/shivamraj/Documents/Learning/pytest/palindrome/resources/expected_file_two.txt", "r")
    examples = file.read()
    return examples


def test_is_not_palindrome_from_file_two(input_from_file_two, output_from_file_two):
    assert str(pal.is_palindrome(input_from_file_two)) == output_from_file_two


@pytest.mark.parametrize("non_palindrome", [
    "abcd",
    "ababab",
])
def test_is_palindrome_not_palindrome(non_palindrome):
    assert not pal.is_palindrome(non_palindrome)


@pytest.mark.parametrize("maybe_palindrome, expected_result", [
    ("", True),
    ("A", True),
    ("Bob", True),
    ("Never odd or even", True),
    ("Do geese see God?", True),
    ("abcd", False),
    ("ababab", False),
])
def test_is_palindrome(maybe_palindrome, expected_result):
    assert pal.is_palindrome(maybe_palindrome) == expected_result


@pytest.mark.parametrize("example, expected", [
    ('deleveled', True),
    ('Malayalam', True),
    ('detartrated', True),
    ('a', True),
    ('repaper', True),
    ('Al lets Della call Ed Stella', True),
    ('Lisa Bonet ate no basil', True),
    ('Linguistics', False),
    ('Python', False),
    ('palindrome', False),
    ('an', False),
    ('re-paper', True)
])
def test_palindrome_detector_looping_(example, expected):
    result = pal.is_palindrome(example)
    assert result == expected
